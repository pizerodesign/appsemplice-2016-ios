//
//  RA_NewsViewController.h
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RA_NewsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,UIWebViewDelegate>
{
    IBOutlet UITableView *containerTableview;
}
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
