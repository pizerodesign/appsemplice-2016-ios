//
//  RA_NewsViewController.m
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_NewsViewController.h"
#import "RA_NewsDetailViewController.h"
#import "RA_NewsObject.h"
#import "Reachability.h"
#import "ASIDownloadCache.h"

@interface RA_NewsViewController () <NSXMLParserDelegate>
{
    NSXMLParser *parser;
    NSString *element;
    NSString *tempelEment;
    
    NSMutableString *title,*link,*pubDate;
    
    RA_NewsObject *newsObject;
    BOOL dataLoaded;
}

@property (nonatomic, retain) NSMutableArray *newsObjectArray;

@property (nonatomic, retain) ASIHTTPRequest *newsRequest;

@end

@implementation RA_NewsViewController

@synthesize newsObjectArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable){
        
        UIAlertView* alert = [[UIAlertView alloc]initWithTitle:@"Info" message:@"Senza connessione è disponibile la cronologia dei contenuti precedentemente visitati. Per visualizzare nuovi contenuti attiva la tua connessione internet." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    _webView.delegate = self;
    
   
          [super viewDidLoad];
   /* NSString *fullURL =  [[NSUserDefaults standardUserDefaults]objectForKey:@"WebView_Address"];//@"http://m.facebook.com/";
        NSURL *url = [NSURL URLWithString:fullURL];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [_webView loadRequest:requestObj]; */
}


-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
}

-(void)webViewDidStartLoad:(UIWebView *)webView{

    //show busy screen
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = AMLocalizedString(@"kWait", nil);
    hud.labelColor = [UIColor lightGrayColor];


}

-(void)webViewDidFinishLoad:(UIWebView *)webView{


    //busy screen get hidden
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    //if parsing is already done, do not need to parse again
    if(!dataLoaded)
    {
        dataLoaded = YES;
        [self fetchNewsFeed];
    }
}

-(void)fetchNewsFeed
{
    //creating the parser and start parsing
    parser = [[NSXMLParser alloc] initWithContentsOfURL:[NSURL URLWithString:NewsFeedAPI]];
    [parser setDelegate:self];
    [parser setShouldResolveExternalEntities:NO];
    [parser parse];
}

-(void)reloadButtonPressed
{
    //reload button action
    [newsObjectArray removeAllObjects];
    
    //show busy screen
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = AMLocalizedString(@"kWait", nil);
    hud.labelColor = [UIColor lightGrayColor];
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01f]];
    
    //start fetching newsfeed
    [self fetchNewsFeed];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark tableview delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return newsObjectArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //showing feeds list
    static NSString *identifier = @"NewsCellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.font = [UIFont boldSystemFontOfSize:13.0f];
    }
    RA_NewsObject *news = [newsObjectArray objectAtIndex:indexPath.row];
    cell.textLabel.text = news.titleString;
    cell.detailTextLabel.text = news.publishedDateString;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // showing the tapped feed's link to the next page
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    RA_NewsObject *news = [newsObjectArray objectAtIndex:indexPath.row];
    
    RA_NewsDetailViewController *vc = [[RA_NewsDetailViewController alloc] initWithNibName:@"RA_NewsDetailViewController" bundle:nil withUrlString:news.linkString];
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: AMLocalizedString(@"kBack", nil) style: UIBarButtonItemStyleBordered target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark parser delegate methods

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    element = elementName;
    if([element isEqualToString:@"item"])
    {
        //detecting each item in the xml
        newsObject = [[RA_NewsObject alloc] init];
        
        title = [[NSMutableString alloc] init];
        link = [[NSMutableString alloc] init];
        pubDate = [[NSMutableString alloc] init];
    }
}

-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if([element isEqualToString:@"title"])//detecting title of a feed
    {
        [title appendString:string];
    }
    else if ([element isEqualToString:@"link"])//detecting link of a feed
    {
        if(![string isEqualToString:@" "])
        {
            [link appendString:string];
        }
    }
    else if([element isEqualToString:@"pubDate"])//detecting pubDate of a feed
    {
        [pubDate appendString:string];
    }
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:@"item"])
    {
        // a news object consist of a feed's url,title and publishing date
        newsObject.titleString = title;
        newsObject.publishedDateString = pubDate;
        newsObject.linkString = link;
        
        [newsObjectArray addObject:newsObject];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser
{
    // parsing ended and hide the busy screen and reload table
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    [containerTableview reloadData];
}



@end
