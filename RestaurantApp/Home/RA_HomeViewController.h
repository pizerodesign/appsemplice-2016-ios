//
//  RA_HomeViewController.h
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface RA_HomeViewController : UIViewController <UITableViewDataSource, UITableViewDelegate,ASIHTTPRequestDelegate>
{
    IBOutlet UITableView *containerTableView;
    IBOutlet UIImageView *slideshowImageView;
    IBOutlet UIView *slightShowBGView;
    
    IBOutlet UIButton *leftButton;
    IBOutlet UIButton *rightButton;
}

-(IBAction)leftButtonAction:(UIButton*)sender;
-(IBAction)rightButtonAction:(UIButton*)sender;


@end
