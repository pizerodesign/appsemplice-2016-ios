//
//  RA_HomeViewController.m
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_HomeViewController.h"
#import "RA_RestaurantViewController.h"
#import "RA_NewsViewController.h"
#import "RA_TakeAwayViewController.h"
#import "RA_ReservationViewController.h"
#import "RA_SettingsViewController.h"
#import "RA_HomeCell.h"
#import "RA_GalleryViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface RA_HomeViewController () <SettingsViewControllerDelegate>
{
    NSArray *tableViewCellNames;
    NSArray *slideshowImageArray;
    
    int indexOfSlideShowImageArray;
    
    RA_HomeCell *restaurantPageCell;
    RA_HomeCell *imageGalleryCell;
    RA_HomeCell *newsCell;
    
    //new code
    RA_ImageCache *imgCh;
    IBOutlet UIImageView *bg_imageView;
}

//new code
@property (nonatomic, retain) ASIHTTPRequest *galleryRequest;
@property (nonatomic, retain) NSArray *gallaryItems;
@property (nonatomic, assign) BOOL isImagesShowed;

@end

@implementation RA_HomeViewController

//new
@synthesize galleryRequest;
@synthesize gallaryItems = _gallaryItems;
@synthesize isImagesShowed;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //new code
    
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Home_bg_Image"];
    UIImage* image = [UIImage imageWithData:imageData];
    bg_imageView.image = image;
    
     self.isImagesShowed = NO;
    //custom class created to store images into cache so that user do not have to wait each time when the images are to be loaded
    imgCh = [[RA_ImageCache alloc] init];
    

    
    //request server for images which are to be shown
    NSMutableString *urlStr = [[NSMutableString alloc] init];
    [urlStr appendFormat:@"%@?accesskey=%@&entry_by=%@", galleryAPI,AccessKey,kEntryBy];
    NSURL *url = [NSURL URLWithString:urlStr];
    self.galleryRequest = [ASIHTTPRequest requestWithURL:url];
    //cache code added
    [self.galleryRequest setDownloadCache:[ASIDownloadCache sharedCache]];
    [self.galleryRequest setCachePolicy:ASIAskServerIfModifiedCachePolicy|ASIFallbackToCacheIfLoadFailsCachePolicy];
    [self.galleryRequest setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    //end
    galleryRequest.delegate = self;
    [galleryRequest startAsynchronous];
    
    //end new code
    
    self.navigationController.navigationBar.translucent = NO;
    //defining the settings button
    UIBarButtonItem *settingsButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"settings.png"] style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonAction)];
    settingsButton.tintColor = [UIColor whiteColor];
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.navigationController.navigationBar.barTintColor = [RA_UserDefaultsManager colorFromHexString:[[NSUserDefaults standardUserDefaults]objectForKey:@"Color_Primary"]];//kNavigationBarColor;
    }
    else
    {
        self.navigationController.navigationBar.tintColor = kNavigationBarColor;
    }
    self.navigationItem.rightBarButtonItem = settingsButton;
    
    // defining call button
    UIBarButtonItem *callButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"callButton.png"] style:UIBarButtonItemStylePlain target:self action:@selector(callButtonAction)];
    callButton.tintColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = callButton;
    
    indexOfSlideShowImageArray = 0;
    
    // name of the features in home page initialised
    tableViewCellNames = [[NSArray alloc] initWithObjects:@"Our Restaurant",@"The Menu",@"News & Offers",@"Take Away",@"Reservation",nil];
    
    // initial picture for the slideshow
   // slideshowImageView.image = [UIImage imageNamed:@"cover_slide4.png"];
    
    //populating the imageArray with images for showing slideshow
   // slideshowImageArray = [[NSArray alloc] initWithObjects:@"cover_slide1.jpg",@"cover_slide2.png",@"cover_slide3.png",@"cover_slide4.png", nil];
    
    // a timer created to autoswitch the images in the slideshow
    NSTimer *slideshowTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(commitSlideshow) userInfo:nil repeats:YES];
    [slideshowTimer fire];
    
    //the tableview, containing the features of the this page, attributes are changed according to docs provided
    containerTableView.separatorColor = [UIColor clearColor];
    containerTableView.layer.cornerRadius = 4.0f;
    containerTableView.layer.borderWidth = 1.0f;
    containerTableView.layer.borderColor = kBorderColor;
    containerTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    containerTableView.backgroundColor = [UIColor whiteColor];
    
    // handling view for 3.5 display
    if([UIScreen mainScreen].bounds.size.height < 568)
    {
        CGRect frame = containerTableView.frame;
        frame.size.height = 150;
        containerTableView.frame = frame;
    }
    
    //setting view background
    self.view.backgroundColor = kPageBGColor;
    [containerTableView setBackgroundColor:kPageBGColor];
    slideshowImageView.backgroundColor = kPageBGColor;
    
    // changing attributes to the slideshowview
    slightShowBGView.layer.borderWidth = 1.0f;
    slightShowBGView.layer.borderColor = kBorderColor;
    slightShowBGView.layer.cornerRadius = 4.0f;
    
    [self cellConfiguration];
    
    if([UIScreen mainScreen].bounds.size.height > 568) // this scope is executed when the device is iPad and and the view is re-arranged
    {
       
        CGRect frameTable = containerTableView.frame;
        frameTable.size.height = 300;
        containerTableView.frame = frameTable;
        
       /* CGRect frameImageView = slideshowImageView.frame;
        frameImageView.size.height = 400;
        slideshowImageView.frame = frameImageView;
        
        CGRect frameImageBGView = slightShowBGView.frame;
        frameImageBGView.size.height = 410;
        slightShowBGView.frame = frameImageBGView;
        
        CGRect frameLeftButton = leftButton.frame;
        frameLeftButton.size.width = 25;
        frameLeftButton.size.height = 25;
        frameLeftButton.origin.y = slideshowImageView.frame.size.height / 2 - frameLeftButton.size.height / 2;
        leftButton.frame = frameLeftButton;
        leftButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        leftButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        
        CGRect frameRightButton = rightButton.frame;
        frameRightButton.origin.x -= 10;
        frameRightButton.size.width = 25;
        frameRightButton.size.height = 25;
        frameRightButton.origin.y = slideshowImageView.frame.size.height / 2 - frameRightButton.size.height / 2;
        rightButton.frame = frameRightButton;
        rightButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        rightButton.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
        
        CGRect frameTable = containerTableView.frame;
        frameTable.origin.y = 450;
        frameTable.size.height = 300;
        containerTableView.frame = frameTable;*/
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    //localizing texts according to user chosen language
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    [self changeLanguage];
}

//new code

-(void)requestStarted:(ASIHTTPRequest *)request{
slideshowImageView.image = [UIImage imageNamed:@"cover_slide4.png"];

}
-(void)requestFinished:(ASIHTTPRequest *)request
{
    //if server request succeeded
    if(request == self.galleryRequest)
    {
        NSError *error = nil;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error];
        NSLog(@"responseObject %@", responseObject);
        if (error)
        {
            LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kParseResponse", nil)  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            
            return;
        }
        
        NSArray *responseArr = [responseObject objectForKey:@"data"];
        NSArray *responseArray = [[responseArr reverseObjectEnumerator] allObjects];
        NSMutableArray *itemsArray = [[NSMutableArray alloc] init];
        for(int i=0; i<4; i++)
        {
            NSDictionary *dic = [[responseArray objectAtIndex:i] objectForKey:@"Gallery"];
            
            RA_GalleryObject *galleryObject = [[RA_GalleryObject alloc] init];
            //a galleryobject contains an image and the id of the image sent from server
            galleryObject.objectId = [[dic objectForKey:@"ID"] intValue];
            galleryObject.imagePath = [dic objectForKey:@"Image_Path"];
            galleryObject.fullImagePath = [dic objectForKey:@"Image_Path"];
            galleryObject.fullImagePath = [NSString stringWithFormat:@"%@%@",kImageBaseUrl,[dic objectForKey:@"Image_Path"]];
            
            [itemsArray addObject:galleryObject];
        }
        
        _gallaryItems = [[NSArray alloc] initWithArray:itemsArray];
        NSMutableArray *images = [[NSMutableArray alloc] init];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
            for(RA_GalleryObject *object in _gallaryItems)
            {
                // creating a thread where images are fetched from server,stored in cache and then populated in an array
                NSMutableString *urlStr = [[NSMutableString alloc] init];
                [urlStr appendFormat:@"%@%@",kImageBaseUrl,object.imagePath];
                object.thumbImage = [imgCh getImage:urlStr];
                
                [images addObject:object.thumbImage];
            }
        
        slideshowImageArray = [[NSArray alloc] initWithArray:images];
           // [containerTableView reloadData];
            
            // hide busy screen
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            self.isImagesShowed = YES;
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
             slideshowImageArray = [[NSArray alloc] initWithArray:images];
            //[containerTableView reloadData];
        });
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    // could not connect to server
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kConnectServer", nil) delegate:Nil cancelButtonTitle:AMLocalizedString(@"kDismiss", nil) otherButtonTitles: nil];
    [alert show];
    
    
}

//end new code

-(void)callButtonAction
{
    //call button funtionality
    if([[UIDevice currentDevice].model isEqualToString:@"iPhone"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",kPhone]]];
    }
    else
    {
        // case if call functionality not present in the device
        LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops!" message:AMLocalizedString(@"kPhoneCall", nil) delegate:nil cancelButtonTitle:AMLocalizedString(@"kDismiss", nil) otherButtonTitles:nil];
        [alert show];
    }
}

-(void)settingsButtonAction
{
    // settings button action, opens the setting page
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    RA_SettingsViewController *vc = [[RA_SettingsViewController alloc] initWithNibName:@"RA_SettingsViewController" bundle:nil];
    vc.title = AMLocalizedString(@"kSettings", nil);
    vc.delegate = self;
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: AMLocalizedString(@"kBack", nil) style: UIBarButtonItemStyleBordered target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)updateBackButton
{
    // configuring the back button
    UIBarButtonItem *newBackButton = (UIBarButtonItem*) [[self navigationItem] backBarButtonItem];
    newBackButton.title = AMLocalizedString(@"kBack", nil);
}

-(void)commitSlideshow
{
    // slideshow animation, image trasition handled through CATransition
    slideshowImageView.image = [slideshowImageArray objectAtIndex:indexOfSlideShowImageArray];//[UIImage imageNamed:[slideshowImageArray objectAtIndex:indexOfSlideShowImageArray]];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 1.0f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [slideshowImageView.layer addAnimation:transition forKey:nil];
    
    if(indexOfSlideShowImageArray == 3)
        indexOfSlideShowImageArray = -1;
    indexOfSlideShowImageArray++;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark TableView Delegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // showing feature cells at approprite position
    switch (indexPath.row)
    {
        case 0:
            return restaurantPageCell;
            
        case 1:
            return imageGalleryCell;
            
        case 2:
            return newsCell;
            
        default:
            break;
    }
    
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //opening approprite features tapping on feature cells
    [containerTableView deselectRowAtIndexPath:indexPath animated:YES];
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);

    UIViewController *vc;
    if(indexPath.row == 0)
    {
        vc = [[RA_RestaurantViewController alloc] initWithNibName:@"RA_RestaurantViewController" bundle:nil];
        vc.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Main"];
    }
    else if(indexPath.row == 1)
    {
        vc = [[RA_GalleryViewController alloc] initWithNibName:@"RA_GalleryViewController" bundle:nil];
        vc.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Gallery"];
    }
    else if(indexPath.row == 2)
    {
        vc = [[RA_NewsViewController alloc] initWithNibName:@"RA_NewsViewController" bundle:nil];
        vc.title =  [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_WebView"];//AMLocalizedString(@"kNews_Offers", nil);
    }
    
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: AMLocalizedString(@"kBack", nil) style: UIBarButtonItemStyleBordered target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //handling cases for height of each feature cells for ipad,ipohone 4 inch and iphone3.5inch
    if([UIScreen mainScreen].bounds.size.height < 568)
    {
        return 50;
    }
    if([UIScreen mainScreen].bounds.size.height > 568)
    {
        return 100;
    }
    return 63;
}

-(IBAction)leftButtonAction:(UIButton*)sender
{
    // swithing image to previous image in slideshow
    if(indexOfSlideShowImageArray <= 0)
        indexOfSlideShowImageArray = 4;
    indexOfSlideShowImageArray--;
    
    slideshowImageView.image = [slideshowImageArray objectAtIndex:indexOfSlideShowImageArray];//[UIImage imageNamed:[slideshowImageArray objectAtIndex:indexOfSlideShowImageArray]];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.2f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [slideshowImageView.layer addAnimation:transition forKey:nil];
}

-(IBAction)rightButtonAction:(UIButton*)sender
{
    // swithing image to next image in slideshow
    if(indexOfSlideShowImageArray == 3)
        indexOfSlideShowImageArray = -1;
    indexOfSlideShowImageArray++;
    
    slideshowImageView.image = [slideshowImageArray objectAtIndex:indexOfSlideShowImageArray];//[UIImage imageNamed:[slideshowImageArray objectAtIndex:indexOfSlideShowImageArray]];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.2f;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionFade;
    [slideshowImageView.layer addAnimation:transition forKey:nil];
}

-(void)cellConfiguration
{
    // configuring static cells
    restaurantPageCell = [[RA_HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomePageCellId"];
    restaurantPageCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Main"];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Thumb_1"];
    UIImage* image = [UIImage imageWithData:imageData];
    restaurantPageCell.pageImageView.image = image;//[UIImage imageNamed:@"cell1.png"];
    
    imageGalleryCell = [[RA_HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomePageCellId"];
    imageGalleryCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Gallery"];
    NSData* imageData2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Thumb_2"];
    UIImage* image2 = [UIImage imageWithData:imageData2];
    imageGalleryCell.pageImageView.image = image2;//[UIImage imageNamed:@"cell2.png"];
    
    newsCell = [[RA_HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"HomePageCellId"];
    newsCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_WebView"];
    NSData* imageData3 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Thumb_3"];
    UIImage* image3 = [UIImage imageWithData:imageData3];
    newsCell.pageImageView.image = image3;//[UIImage imageNamed:@"cell3.png"];
}

-(void)changeLanguage
{
    // localizing static strings according to user chosen language
    restaurantPageCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Main"];
    imageGalleryCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Gallery"];
    newsCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_WebView"];
    
    [containerTableView reloadData];
}

@end
