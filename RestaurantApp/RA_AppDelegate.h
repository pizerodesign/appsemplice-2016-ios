//
//  RA_AppDelegate.h
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RA_RestaurantViewController.h"
#import "RA_NewsViewController.h"
#import "RA_TakeAwayViewController.h"
#import "RA_HomeViewController.h"
#import "RA_ItemViewController.h"
#import "RA_FindUsViewController.h"
	//#import "RA_ReservationParentViewController.h"
#import "RA_ReservationViewController.h"
#import "RA_CategoryViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "RA_MenuObject.h"
#import "ASIHTTPRequest.h"
#import "ASIDownloadCache.h"
#import "RA_UserDefaultsManager.h"
#import <OneSignal/OneSignal.h>

//#define UIColorFromRGB(rgbValue) \
//[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
//green:((float)((rgbValue & 0x00FF00) >>  8))/255.0 \
//blue:((float)((rgbValue & 0x0000FF) >>  0))/255.0 \
//alpha:1.0]

#define ColorFromHexString(hexString)\
unsigned rgbValue = 0;\
NSScanner *scanner = [NSScanner scannerWithString:hexString];\
[scanner setScanLocation:1];\
[scanner scanHexInt:&rgbValue];\
return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];


@interface RA_AppDelegate : UIResponder <UIApplicationDelegate, UITabBarControllerDelegate, UIAlertViewDelegate, ASIHTTPRequestDelegate,ASICacheDelegate,NSURLConnectionDelegate>
{
    UINavigationController *navigationController;

    RA_RestaurantViewController *restaurantVC;
    RA_CategoryViewController *menuVC;
    RA_NewsViewController *newsVC;
    RA_TakeAwayViewController *takeAwayVC;
		//RA_ReservationParentViewController *reservationVC;
	RA_ReservationViewController *reservationVC;
    RA_HomeViewController *homeVC;
    RA_FindUsViewController *findUsVC;
    
    UINavigationController *homeNC;
    UINavigationController *menuNC;
    UINavigationController *reservationNC;
    UINavigationController *findUsNC;
    
    UITabBarController *tabController;
    
    //Api connection
    NSMutableData *responseData;
    NSURLConnection *connection;
    NSArray *results;
    
    //push notifications
    OneSignal *oneSignal;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, readwrite) float taxAmount;
@property (nonatomic, retain) NSString *currency;
@property (nonatomic, retain) ASIHTTPRequest *taxCurrencyRequest;

-(void)changeLanguages;

@end
