//
//  RA_ReservationParentViewController.m
//  RestaurantApp
//
//  Created by World on 12/26/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_ReservationParentViewController.h"
#import "RA_TakeAwayViewController.h"
#import "RA_ReservationViewController.h"
#import "RA_HomeCell.h"

@interface RA_ReservationParentViewController ()
{
    RA_HomeCell *takeAwayCell;
    RA_HomeCell *reservationCell;
}

@end

@implementation RA_ReservationParentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.navigationController.navigationBar.barTintColor = [RA_UserDefaultsManager colorFromHexString:[[NSUserDefaults standardUserDefaults]objectForKey:@"Color_Primary"]];
    }
    else
    {
        self.navigationController.navigationBar.tintColor = kNavigationBarColor;
    }
    
    //tableview attributes modified
    containerTableView.separatorColor = [UIColor clearColor];
    containerTableView.layer.cornerRadius = 4.0f;
    containerTableView.layer.borderWidth = 1.0f;
    containerTableView.layer.borderColor = kBorderColor;
    containerTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    containerTableView.backgroundColor = [UIColor whiteColor];
    
    //background color of the page and tableview set
    self.view.backgroundColor = kPageBGColor;
    [containerTableView setBackgroundColor:kPageBGColor];
    
    [self cellConfiguration];
    
    if([UIScreen mainScreen].bounds.size.height > 568)//tableview frame for ipad
    {
        CGRect frame = containerTableView.frame;
        frame.size.height = 200;
        containerTableView.frame = frame;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateStrings];//localize static strings to user selected language
    [containerTableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark tableview delegate methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
		//return 2;
	return 1;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    showing takeAwayCell and reservationCell
    switch (indexPath.row)
    {
		//case 0:
		//return reservationCell;
        
        case 0:
            return takeAwayCell;
            
        default:
            break;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([UIScreen mainScreen].bounds.size.height > 568)//ipad
    {
        return 100;
    }
    else return 63.0f;//iphone
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //open the page for which cell has been selected
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    
    UIViewController *vc = nil;
    if(indexPath.row == 0)
    {
        vc = [[RA_ReservationViewController alloc] initWithNibName:@"RA_ReservationViewController" bundle:nil];
        vc.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Reserve"];
    }
    else
    {
        vc = [[RA_TakeAwayViewController alloc] initWithNibName:@"RA_TakeAwayViewController" bundle:nil];
        vc.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Order"];
    }
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle: AMLocalizedString(@"kBack", nil) style: UIBarButtonItemStyleBordered target: nil action: nil];
    [[self navigationItem] setBackBarButtonItem: newBackButton];
    
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)cellConfiguration
{
    //configuring static cells.
    takeAwayCell = [[RA_HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reservationCellId"];
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Thumb_4"];
    UIImage* image = [UIImage imageWithData:imageData];
    takeAwayCell.pageImageView.image = image;//[UIImage imageNamed:@"cell1.png"];
    
    reservationCell = [[RA_HomeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reservationCellId"];
    NSData* imageData2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Thumb_5"];
    UIImage* image2 = [UIImage imageWithData:imageData2];
    reservationCell.pageImageView.image = image2;//[UIImage imageNamed:@"cell2.png"];
    
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    takeAwayCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Order"];//AMLocalizedString(@"kTakeAway", nil);
    reservationCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Reserve"];
}

-(void)updateStrings
{
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    takeAwayCell.pageNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Order"];//AMLocalizedString(@"kTakeAway", nil);
    reservationCell.pageNameLabel.text =  [[NSUserDefaults standardUserDefaults]objectForKey:@"Text_Reserve"];
}

@end
