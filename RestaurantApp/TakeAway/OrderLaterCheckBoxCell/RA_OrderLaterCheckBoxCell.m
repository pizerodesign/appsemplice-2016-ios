//
//  RA_OrderLaterCheckBoxCell.m
//  RestaurantApp
//
//  Created by World on 12/20/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_OrderLaterCheckBoxCell.h"
#import "RA_UserDefaultsManager.h"

@implementation RA_OrderLaterCheckBoxCell

@synthesize delegate;
@synthesize isCheckBoxSelected;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier isCheckboxSelected:(BOOL)isSelected
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.isCheckBoxSelected = isSelected;
        
      //  orderLaterLabel = [[UILabel alloc] init];
      //  orderLaterLabel.text = @"Check if you want to order later";
      //  orderLaterLabel.textColor = [UIColor grayColor];
      //  orderLaterLabel.backgroundColor = [UIColor clearColor];
      //  [self.contentView addSubview:orderLaterLabel];
        
      //  checkBoxImageView = [[UIImageView alloc] init];
        
        if(isSelected)
        {
//            [checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxSelected.png"]];
            
            [RA_UserDefaultsManager removeAllItems];
            
        }
        else
        {
//            [checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxDeselected.png"]];
        }
        [self.contentView addSubview:checkBoxImageView];
        
        checkBoxButton = [[UIButton alloc] init];
        checkBoxButton.backgroundColor = [UIColor colorWithRed:(160/255.0) green:(160/255.0) blue:(160/255.0) alpha:1];


        [checkBoxButton setTitle:AMLocalizedString(@"kOrderLaterCheckBox", nil) forState:UIControlStateNormal];
        checkBoxButton.titleLabel.font = [UIFont systemFontOfSize:15.0f];

        [checkBoxButton addTarget:self action:@selector(checkBoxButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:checkBoxButton];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat x = self.contentView.bounds.origin.x;
    CGFloat y = self.contentView.bounds.origin.y;
    CGFloat h = self.contentView.bounds.size.height;
    CGFloat w = self.contentView.bounds.size.width;
    
    //alligning the button
    if([UIScreen mainScreen].bounds.size.height > 568)
    {
        checkBoxButton.frame = CGRectMake(x + 10, y + 5, w - 20, h - 10);
    }
    else
    {
        checkBoxButton.frame = CGRectMake(x + 10, y + 5, w - 20, h - 10);
    }

    checkBoxButton.layer.cornerRadius = 5.0;
    checkBoxImageView.frame = CGRectMake(x + 20, h/2 - 12, 24, 24);
    //checkBoxButton.frame = self.contentView.bounds;//CGRectMake(x + 20, h/2 - 12, 24, 24);
    orderLaterLabel.frame = CGRectMake(x + 60, y, 300, h);
}

-(void)checkBoxButtonTapped
{
    self.isCheckBoxSelected = !self.isCheckBoxSelected;
    if(self.isCheckBoxSelected)
    {
       // [checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxSelected.png"]];
        // [RA_UserDefaultsManager removeAllItems];
       
    }
    else
    {
       // [checkBoxImageView setImage:[UIImage imageNamed:@"checkBoxDeselected.png"]];
    }
    
    if(delegate)
    {
        [delegate checkBoxButtonSelected:self.isCheckBoxSelected];
    }
}

-(void)changeLabel
{
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    orderLaterLabel.text = AMLocalizedString(@"kOrderLaterCheckBox", nil);
    
     [checkBoxButton setTitle:AMLocalizedString(@"kOrderLaterCheckBox", nil) forState:UIControlStateNormal];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
