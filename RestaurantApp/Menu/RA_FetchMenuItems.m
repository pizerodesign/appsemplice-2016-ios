//
//  RA_FetchMenuItems.m
//  RestaurantApp
//
//  Created by World on 12/30/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_FetchMenuItems.h"
#import "RA_MenuObject.h"

@implementation RA_FetchMenuItems
{
    int totalRequests;
    int totalCategoryId;
}

@synthesize menuItemRequest;
@synthesize delegate;

-(id)init
{
    self = [super init];
    if(self)
    {
        totalRequests = 0;
        menuItemArray = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)getMenuItemsForCategoryIdArray:(NSArray*)categoryIdArray
{
    totalCategoryId = (int)categoryIdArray.count;
    for(int i = 0; i < categoryIdArray.count; i++)
    {
        NSMutableString *urlStr = [[NSMutableString alloc] init];
        [urlStr appendFormat:@"%@?accesskey=%@&category_id=%@&entry_by=%@", MenuAPI,AccessKey,[categoryIdArray objectAtIndex:i], kEntryBy];
        
        NSURL *url = [NSURL URLWithString:urlStr];
        self.menuItemRequest = [ASIHTTPRequest requestWithURL:url];
        menuItemRequest.delegate = self;
        
        [menuItemRequest startAsynchronous];
    }
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    totalRequests++;
    
    NSError *error = nil;
    NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:request.responseData options:kNilOptions error:&error];
    if (error)
    {
        LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kParseResponse", nil)  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        return;
    }
    
    NSArray *responseArray = [responseObject objectForKey:@"data"];
    
    for(int i = 0; i < responseArray.count; i++)
    {
        NSDictionary *dic = [[responseArray objectAtIndex:i] objectForKey:@"Menu"];
        
        RA_MenuObject *menuObject = [[RA_MenuObject alloc] init];
        menuObject.menuId = [dic objectForKey:@"Menu_ID"];
        menuObject.menuImagePath = [dic objectForKey:@"Menu_image"];
        menuObject.menuName = [dic objectForKey:@"Menu_name"];
        menuObject.menuPrice = [dic objectForKey:@"Price"];
        
        [menuItemArray addObject:menuObject];
    }
    
    if(totalCategoryId == totalRequests)
    {
        if(delegate)
        {
            [delegate menuItemFetchingCompleted:menuItemArray];
        }
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kConnectServer", nil) delegate:Nil cancelButtonTitle:AMLocalizedString(@"kDismiss", nil) otherButtonTitles: nil];
    [alert show];
}

-(NSArray*)getMenuItems
{
    return menuItemArray;
}

@end
