//
//  RA_FetchMenuItems.h
//  RestaurantApp
//
//  Created by World on 12/30/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol fetchMenuItemDelegate <NSObject>

-(void)menuItemFetchingCompleted:(NSArray*)menuItems;

@end

@interface RA_FetchMenuItems : NSObject <ASIHTTPRequestDelegate>
{
    NSMutableArray *menuItemArray;
}

@property (nonatomic, retain) ASIHTTPRequest *menuItemRequest;
@property (nonatomic, retain) id <fetchMenuItemDelegate> delegate;

-(void)getMenuItemsForCategoryIdArray:(NSArray*)categoryIdArray;
-(NSArray*)getMenuItems;

@end
