//
//  RA_AppDelegate.m
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RA_AppDelegate.h"
#import "RA_MessageBoard.h"
#import "Reachability.h"
#import "RNCachingURLProtocol.h"


// return true if the device has a retina display, false otherwise
#define IS_RETINA_DISPLAY() [[UIScreen mainScreen] respondsToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0f

// return the scale value based on device's display (2 retina, 1 other)
#define DISPLAY_SCALE IS_RETINA_DISPLAY() ? 2.0f : 1.0f

// if the device has a retina display return the real scaled pixel size, otherwise the same size will be returned
#define PIXEL_SIZE(size) IS_RETINA_DISPLAY() ? CGSizeMake(size.width/2.0f, size.height/2.0f) : size

@implementation RA_AppDelegate

@synthesize taxAmount = _taxAmount;
@synthesize currency = _currency;
@synthesize taxCurrencyRequest;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    
    //cache code added
    
    [NSURLProtocol registerClass:[RNCachingURLProtocol class]];
    
    [ASIHTTPRequest setDefaultCache:[ASIDownloadCache sharedCache]];
    
    
    // Status Bar set to light
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];

    //[GMSServices provideAPIKey:GoogleMapAPIKey];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    NSArray * langs = [NSLocale preferredLanguages];
    NSString * lang = langs.count ? [langs objectAtIndex: 0] : nil;
    
    if([lang hasPrefix: @"it"])
    {
        [RA_UserDefaultsManager setLanguageToItalian:YES];
        [RA_UserDefaultsManager setAppLanuage:@"it"];
        
        //[self requestURL:@"http://pannello.appsemplice.it/api/api.php?language=it_IT&app_ID=%@", kEntryBy];
        
        NSMutableString *reqURL = [[NSMutableString alloc] init];
        [reqURL appendFormat:@"http://pannello.appsemplice.it/api/api.php?language=it_IT&app_ID=%@", kEntryBy];
        [self requestURL:reqURL];


    }else{

       // [self requestURL:@"http://pannello.appsemplice.it/api/api.php?lapp_ID=0language=en_US"];
        NSMutableString *reqURL = [[NSMutableString alloc] init];
        
        [reqURL appendFormat:@"http://pannello.appsemplice.it/api/api.php?language=en_US&app_ID=%@", kEntryBy];
        [self requestURL:reqURL];

    }
 
    
    [self fetchTaxCurrency];
    
    if([RA_UserDefaultsManager getOrderItemsArray].count > 0)
    {
        LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kConfirm", nil) message:AMLocalizedString(@"kRemoveOrder", nil) delegate:self cancelButtonTitle:AMLocalizedString(@"kNo", nil) otherButtonTitles:AMLocalizedString(@"kYes", nil), nil];
        [alert show];
    }
    
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8.0"))
    {
        if([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
        {
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //one signal push notifications settings
    
    oneSignal = [[OneSignal alloc]
                      initWithLaunchOptions:launchOptions
                      appId:@"f49c29b7-f7c9-4bbc-9eee-f0cb3904680f"
                      handleNotification:^(NSString* message, NSDictionary* additionalData, BOOL isActive) {
                          NSLog(@"OneSignal Notification opened:\nMessage: %@", message);
                          
                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                          [alert show];
                          
                          if (additionalData) {
                              NSLog(@"additionalData: %@", additionalData);
                              
                              // Check for and read any custom values you added to the notification
                              // This done with the "Additonal Data" section the dashbaord.
                              // OR setting the 'data' field on our REST API.
                              NSString* customKey = additionalData[@"customKey"];
                              if (customKey)
                                  NSLog(@"customKey: %@", customKey);
                          }
                      }];
    
    
    return YES;
}

-(void)startApp{
    
    [GMSServices provideAPIKey:[[NSUserDefaults standardUserDefaults]objectForKey:@"Google_Key"]];

    tabController = [[UITabBarController alloc] init];
    
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    
    homeVC = [[RA_HomeViewController alloc] initWithNibName:@"RA_HomeViewController" bundle:nil];
    homeVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Home_Title"];

    homeNC = [[UINavigationController alloc] initWithRootViewController:homeVC];
    homeNC.navigationBar.translucent = NO;
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:@"Home_Icon"];
    UIImage* homeIcon = [UIImage imageWithData:imageData];
    UIImage *H = [[UIImage alloc] initWithCGImage:homeIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp];
    homeNC.tabBarItem.selectedImage = H;
    homeNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Home_Title"];
    
    
    menuVC = [[RA_CategoryViewController alloc] initWithNibName:@"RA_CategoryViewController" bundle:nil];
    menuVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Shop_Title"];
    
    menuNC = [[UINavigationController alloc] initWithRootViewController:menuVC];
    menuNC.navigationBar.translucent = NO;
    NSData* imageData1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Shop_Icon"];
    UIImage* shopIcon = [UIImage imageWithData:imageData1];
    menuNC.tabBarItem.selectedImage = [[UIImage alloc] initWithCGImage:shopIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp];//kMenuButtonSelected;
    menuNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Shop_Title"];
    
    
		//reservationVC = [[RA_ReservationParentViewController alloc] initWithNibName:@"RA_ReservationParentViewController" bundle:nil];
	reservationVC = [[RA_ReservationViewController alloc] initWithNibName:@"RA_ReservationViewController" bundle:nil];
    reservationVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Ordina_Title"];    reservationNC = [[UINavigationController alloc] initWithRootViewController:reservationVC];
    reservationNC.navigationBar.translucent = NO;
    NSData* imageData2 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Order_Icon"];
    UIImage* orderIcon = [UIImage imageWithData:imageData2];
    reservationNC.tabBarItem.selectedImage = [[UIImage alloc] initWithCGImage:orderIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp];
    reservationNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Ordina_Title"];
    
    
    findUsVC = [[RA_FindUsViewController alloc] initWithNibName:@"RA_FindUsViewController" bundle:nil];
    findUsVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Info_Title"];
    findUsNC = [[UINavigationController alloc] initWithRootViewController:findUsVC];
    findUsNC.navigationBar.translucent = NO;
    NSData* imageData3 = [[NSUserDefaults standardUserDefaults] objectForKey:@"Info_Icon"];
    UIImage* infoIcon = [UIImage imageWithData:imageData3];
    findUsNC.tabBarItem.selectedImage = [[UIImage alloc] initWithCGImage:infoIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp];
    findUsNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Info_Title"];
    
    // tab unselected images original color
        homeNC.tabBarItem.image = [[[UIImage alloc] initWithCGImage:homeIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        menuNC.tabBarItem.image = [[[UIImage alloc] initWithCGImage:shopIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        reservationNC.tabBarItem.image = [[[UIImage alloc] initWithCGImage:orderIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        findUsNC.tabBarItem.image = [[[UIImage alloc] initWithCGImage:infoIcon.CGImage scale:DISPLAY_SCALE orientation:UIImageOrientationUp] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    
    tabController.viewControllers = [NSArray arrayWithObjects:homeNC, menuNC, reservationNC, findUsNC, nil];
    tabController.delegate = self;
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, nil]];
    
  
    [[UINavigationBar appearance] setBarTintColor:[RA_UserDefaultsManager colorFromHexString:[[NSUserDefaults standardUserDefaults]objectForKey:@"Color_Secondary"]]];//kTabBarColor];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
       
    // UITabBar setBarTintColor
    [[UITabBar appearance] setBarTintColor:[RA_UserDefaultsManager  colorFromHexString:[[NSUserDefaults standardUserDefaults]objectForKey:@"Color_Secondary"]]];//kTabBarColor];
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITabBar appearance] setShadowImage:[UIImage imageNamed:@"transparentShadow.png"]];
    tabController.tabBar.backgroundColor = [UIColor clearColor];
    
    [[UITabBarItem appearance]setTitleTextAttributes:@{
    NSForegroundColorAttributeName : [UIColor whiteColor],} forState:UIControlStateNormal];
    
    [[UITabBarItem appearance]setTitleTextAttributes:@{
    NSForegroundColorAttributeName : [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0],} forState:UIControlStateSelected];
    
    
    self.window.rootViewController = tabController;
    
    [self.window makeKeyAndVisible];

}

//api connection


-(void)requestURL:(NSString *)strURL
{
    // Create the request.
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:strURL]];
    
    // Create url connection and fire request
    connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
}


//Delegate methods
- (void)connection:(NSURLConnection*)connection didReceiveResponse:(NSURLResponse *)response
{
    NSLog(@"Did Receive Response %@", response);
    responseData = [[NSMutableData alloc]init];
}
- (void)connection:(NSURLConnection*)connection didReceiveData:(NSData*)data
{
    //NSLog(@"Did Receive Data %@", data);
    [responseData appendData:data];
}
- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error
{
    NSLog(@"Did Fail%@",error.localizedDescription);
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"Did Finish");
    // Do something with responseData
    
   // NSString *strData=[[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
    //convert to json
    NSDictionary *dictData = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableLeaves error:nil];
    results = [dictData objectForKey:@"data"];
    
//    for (NSDictionary *result in results) {
//    //check for update flag
//    }
    [self saveAppSettings];
    [self startApp];
    
}

-(void)saveAppSettings{

    for (NSDictionary *result in results) {
        NSLog(@"results: %@", result);
        
        //set nav bar titles
        [[NSUserDefaults standardUserDefaults]setObject:result[@"title_1"] forKey:@"Home_Title"];
        [[NSUserDefaults standardUserDefaults]setObject:result[@"title_2"] forKey:@"Shop_Title"];
        [[NSUserDefaults standardUserDefaults]setObject:result[@"title_3"] forKey:@"Ordina_Title"];
        [[NSUserDefaults standardUserDefaults]setObject:result[@"title_4"] forKey:@"Info_Title"];
        
        //home bg image
        NSString *bg_imageUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"bg_img"]];
        UIImage *bg_image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:bg_imageUrl]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(bg_image) forKey:@"Home_bg_Image"];
        
//        //slide show starting image
//        NSString *starting_imageUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"starting_img"]];
//        UIImage *starting_image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:starting_imageUrl]]];
//        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(starting_image) forKey:@"Starting_Image"];
        
        //nav and tab colors
        [[NSUserDefaults standardUserDefaults]setObject:result[@"color_primary"] forKey:@"Color_Primary"];
        
        [[NSUserDefaults standardUserDefaults]setObject:result[@"color_secondary"] forKey:@"Color_Secondary"];
        
        // google key
        [[NSUserDefaults standardUserDefaults]setObject:result[@"google_key"] forKey:@"Google_Key"];
        
        
        //tab icons
        NSString *ico_homeUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"ico_home"]];
        UIImage *ico_home = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ico_homeUrl]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(ico_home) forKey:@"Home_Icon"];
        
        
        NSString *ico_infoUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"ico_info"]];
        UIImage *ico_info = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ico_infoUrl]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(ico_info) forKey:@"Info_Icon"];
        
        
        NSString *ico_orderUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"ico_order"]];
        UIImage *ico_order = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ico_orderUrl]]];
        
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(ico_order) forKey:@"Order_Icon"];
        
        
        NSString *ico_shopUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"ico_shop"]];
        UIImage *ico_shop = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:ico_shopUrl]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(ico_shop) forKey:@"Shop_Icon"];
       
        //intro text
        [[NSUserDefaults standardUserDefaults]setObject:result[@"intro_text"] forKey:@"Intro_Text"];
       
       //map coordinate
        [[NSUserDefaults standardUserDefaults]setObject:result[@"lat"] forKey:@"Map_Lat"];
        [[NSUserDefaults standardUserDefaults]setObject:result[@"lon"] forKey:@"Map_Lon"];
         [[NSUserDefaults standardUserDefaults]setObject:result[@"address_text"] forKey:@"Address_Text"];
        
    
        //main image
        NSString *mainUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"main_image"]];
        UIImage *main_image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:mainUrl]]];//[self stringByStrippingHTML:mainUrl]]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(main_image) forKey:@"Main_Image"];
        
        //push key
        [[NSUserDefaults standardUserDefaults]setObject:result[@"push_key"] forKey:@"Push_Key"];
        
        //score
        [[NSUserDefaults standardUserDefaults]setObject:result[@"score"] forKey:@"Score"];
      
        //text gallery
         [[NSUserDefaults standardUserDefaults]setObject:result[@"text_gallery"] forKey:@"Text_Gallery"];
        
        //text main
        [[NSUserDefaults standardUserDefaults]setObject:result[@"text_main"] forKey:@"Text_Main"];
        
        //text order
        [[NSUserDefaults standardUserDefaults]setObject:result[@"text_order"] forKey:@"Text_Order"];
        
        //text reserve
        [[NSUserDefaults standardUserDefaults]setObject:result[@"text_reserve"] forKey:@"Text_Reserve"];
      
       //text webview
        [[NSUserDefaults standardUserDefaults]setObject:result[@"text_webview"] forKey:@"Text_WebView"];
        //web address
        [[NSUserDefaults standardUserDefaults]setObject:result[@"webview_addr"] forKey:@"WebView_Address"];
        
        //thumbs
        NSString *thumb_1Url = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"thumb_1"]];
        UIImage *thumb_1 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_1Url]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(thumb_1) forKey:@"Thumb_1"];
        
         NSString *thumb_2Url = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"thumb_2"]];
        UIImage *thumb_2 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_2Url]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(thumb_2) forKey:@"Thumb_2"];
        
        NSString *thumb_3Url = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"thumb_3"]];
        UIImage *thumb_3 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_3Url]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(thumb_3) forKey:@"Thumb_3"];
        
        NSString *thumb_4Url = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"thumb_4"]];
        UIImage *thumb_4 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_4Url]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(thumb_4) forKey:@"Thumb_4"];
        
        
        NSString *thumb_5Url = [NSString stringWithFormat:@"%@%@",kBaseUrl,result[@"thumb_5"]];
        UIImage *thumb_5 = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:thumb_5Url]]];
        [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(thumb_5) forKey:@"Thumb_5"];
    
        
    }


}

//resize images
- (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

//method to delete html tag form string
-(NSString *)stringByStrippingHTML:(NSString*)str
{
    NSRange r;
    while ((r = [str rangeOfString:@"<[^>]+>" options:NSRegularExpressionSearch]).location     != NSNotFound)
    {
        str = [str stringByReplacingCharactersInRange:r withString:@""];
    }
    return str;
}

-(void)changeLanguages
{
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    
    //set nav bar titles
    homeVC.title =  [[NSUserDefaults standardUserDefaults]objectForKey:@"Home_Title"];
    
    homeNC.tabBarItem.title =  [[NSUserDefaults standardUserDefaults]objectForKey:@"Home_Title"];
    
    menuVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Shop_Title"];
    menuNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Shop_Title"];
    
    reservationVC.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Ordina_Title"];
    
    reservationNC.tabBarItem.title = [[NSUserDefaults standardUserDefaults]objectForKey:@"Ordina_Title"];
    
    findUsVC.title =
    [[NSUserDefaults standardUserDefaults]objectForKey:@"Info_Title"];
    findUsNC.tabBarItem.title =
    [[NSUserDefaults standardUserDefaults]objectForKey:@"Info_Title"];
    
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        return;
    }
    else
    {
        [RA_UserDefaultsManager removeAllItems];
    }
}

-(void)fetchTaxCurrency
{
    NSMutableString *urlStr = [[NSMutableString alloc] init];
    [urlStr appendFormat:@"%@?accesskey=%@&entry_by=0", TaxCurrencyAPI,AccessKey];
    
    NSURL *url = [NSURL URLWithString:urlStr];
    taxCurrencyRequest = [ASIHTTPRequest requestWithURL:url];
    self.taxCurrencyRequest.delegate = self;
    
    //cache code added
    [self.taxCurrencyRequest setDownloadCache:[ASIDownloadCache sharedCache]];
    [self.taxCurrencyRequest setCachePolicy:ASIAskServerIfModifiedCachePolicy|ASIFallbackToCacheIfLoadFailsCachePolicy];
    [self.taxCurrencyRequest setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];
    //end
    
    [self.taxCurrencyRequest startAsynchronous];
}

-(void)requestFinished:(ASIHTTPRequest *)request
{
    if(request == self.taxCurrencyRequest)
    {
        NSError *error = nil;
        NSDictionary *responseObject = [NSJSONSerialization JSONObjectWithData:taxCurrencyRequest.responseData options:kNilOptions error:&error];
        if (error)
        {
            LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kParseResponse", nil)  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        
        NSArray *responseArray = [responseObject objectForKey:@"data"];
        if(responseArray.count == 2)
        {
            NSDictionary *dic = [[responseArray objectAtIndex:0] objectForKey:@"tax_n_currency"];
            NSString *taxValue = [dic objectForKey:@"Value"];
            if (taxValue.floatValue>0) {
                _taxAmount = taxValue.floatValue;
            }
            else _taxAmount = 0;
            
            NSDictionary *dic2 = [[responseArray objectAtIndex:1] objectForKey:@"tax_n_currency"];
            _currency = [dic2 objectForKey:@"Value"];
        }
    }
}

-(void)requestFailed:(ASIHTTPRequest *)request
{
    if ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] == NotReachable){
    
    }
    
    else{
    
    LocalizationSetLanguage([RA_UserDefaultsManager appLanguage]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:AMLocalizedString(@"kError", nil) message:AMLocalizedString(@"kConnectServer", nil) delegate:Nil cancelButtonTitle:AMLocalizedString(@"kDismiss", nil) otherButtonTitles: nil];
        [alert show];}
}

-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{
    switch (tabBarController.selectedIndex)
    {
        case 0:
            [homeVC.navigationController popToRootViewControllerAnimated:YES];
            break;
          
        case 1:
            [menuVC.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        case 2:
            [reservationVC.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        case 3:
            [findUsVC.navigationController popToRootViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //Convert deviceToken to String Type

    const char* data = deviceToken.bytes;
    
    NSMutableString* tokenString = [NSMutableString string];
    
    for (int i = 0; i < deviceToken.length; i++)
    {
        [tokenString appendFormat:@"%02.2hhX", data[i]];
    }
    
    NSLog(@"deviceToken String: %@", tokenString);
    
    [RA_UserDefaultsManager setDeviceToken:tokenString];
    
    if ([[RA_MessageBoard instance] createApplicationEndpoint])
    {
        NSLog(@"Device Endpoint has been created successfully.");
    }
    else
    {
        NSLog(@"Failed to create device endpoint");
    }
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to register with error : %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    application.applicationIconBadgeNumber = 0;
    NSString *msg = [NSString stringWithFormat:@"%@", userInfo];
    NSLog(@"%@",msg);
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
