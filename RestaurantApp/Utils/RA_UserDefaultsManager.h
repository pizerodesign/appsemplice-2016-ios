//
//  RA_UserDefaultsManager.h
//  RestaurantApp
//
//  Created by World on 12/19/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <Foundation/Foundation.h>

// Amazon Keys and ARN
#define ACCESS_KEY_ID                   @"AKIAI6WAWRLM3AU73SSQ"
#define SECRET_KEY                      @"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"
#define PLATFORM_APPLICATION_ARN        @"arn:aws:sns:us-east-1:457637732854:app/APNS/focussrl_ios_production"

// Server access key and Api

#define AccessKey @"513420"
#define AdminPageURL @"http://pannello.appsemplice.it/gestione/"

#define galleryAPI @"http://pannello.appsemplice.it/gestione/api/get-all-gallery-photos.php"
#define CategoryAPI @"http://pannello.appsemplice.it/gestione/api/get-all-category-data.php"
#define MenuAPI @"http://pannello.appsemplice.it/gestione/api/get-menu-data-by-category-id.php"
#define TaxCurrencyAPI @"http://pannello.appsemplice.it/gestione/api/get-tax-and-currency.php"
#define MenuDetailAPI @"http://pannello.appsemplice.it/gestione/api/get-menu-detail.php"
#define SendReservationAPI @"http://pannello.appsemplice.it/gestione/api/add-reservation.php"
#define SendTakeAwayAPI @"http://pannello.appsemplice.it/gestione/api/add-take-away.php"
#define MenuSharingAPI @"http://pannello.appsemplice.it/gestione/menu-sharing.php"

// NewsFeed Api
#define NewsFeedAPI @"http://www.facebook.com/feeds/page.php?format=rss20&id=464032226978953"

// Google maps Api key
#define GoogleMapAPIKey @"AIzaSyAVvY8n1Hy3gn3Npf25JoNER9-h4BPaGY4"

// About US page info 10.50426
#define kLatitude        @"45.572327"
#define kLongitude       @"11.548821"
#define restaurant_name  @"Centro Focus"
#define kAddress         @"S.S. Marosticana 24 (Centro Commerciale), 36100 Vicenza (VI)"
#define kPhone           @"+390444926006"
#define kEmail           @"info@focusestetica.it"

// Localized title Keys
#define kHome @"kHome"
#define kMenu @"kMenu"
#define kReservation @"kReservation"
#define kFindUs @"kFindUs"
#define kRestaurantTitle @"Focus"
#define kMarkerTitle @"Centro Abbronzatura, Estetica e Acconciatura"

#define kMenuTitle @"kMenuTitle"
#define kFindUsTitle @"kFindUsTitle"
#define kReservationTitle @"kReservationTitle"// Social selection option

#define kBaseUrl @"http://pannello.appsemplice.it/uploads/AppImages/"
#define kImageBaseUrl @"http://pannello.appsemplice.it/uploads/images/"
#define kEntryBy @"1" // ID of the App as defined by the backend user assigned

typedef enum {
    kFacebook = 0,
    kTwitter
}kSettingsOptions;

@interface RA_UserDefaultsManager : NSObject

+(CAGradientLayer*)getGradientLayerForBounds:(CGRect)bounds;
+(CAGradientLayer*)getGradientLayerInHomeCellForBounds:(CGRect)bounds;

+(void)setFacebookConnected:(BOOL)isConnected;
+(BOOL)isFacebookConnected;

+(void)setTwitterConnected:(BOOL)isConnected;
+(BOOL)isTwitterConnected;

+(void)setLanguageToItalian:(BOOL)value;
+(BOOL)isLanguageItalian;

+(NSArray*)getOrderItemsArray;
+(void)addMenuItems:(NSArray*)_menuItems;
+(void)removeAllItems;

-(void)setTakeAwaySent:(BOOL)isSent;
-(BOOL)isTakeAwaySent;

+(void)setAppLanuage:(NSString*)language;
+(NSString*)appLanguage;

+(void)setDeviceToken:(NSString*)token;
+(NSString*)deviceToken;

+(void)setDeviceEndPoint:(NSString*)endPoint;
+(NSString*)deviceEndPoint;

+(void)setHasRegisteredDeviceToken:(BOOL)hasRegistered;
+(BOOL)hasRegisteredDeviceToken;

+ (UIColor *)colorFromHexString:(NSString *)hexString;

@end
