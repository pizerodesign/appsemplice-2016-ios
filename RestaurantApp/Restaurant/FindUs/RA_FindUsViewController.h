//
//  RA_FindUsViewController.h
//  RestaurantApp
//
//  Created by World on 12/18/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "GoogleMaps/GoogleMaps.h"

@interface RA_FindUsViewController : UIViewController <MFMailComposeViewControllerDelegate, GMSMapViewDelegate>
{
    IBOutlet UILabel *restaurantNameLabel;
    IBOutlet UILabel *restaurantAddressLabel;
    IBOutlet UILabel *restaurantEmailLabel;
    
    IBOutlet UIButton *callUsButton;
    IBOutlet UIButton *emailUsButton;
    
    CLLocation *location;
}

-(IBAction)callUsButtonAction:(UIButton*)sender;
-(IBAction)emailUsButtonAction:(UIButton*)sender;

@end
