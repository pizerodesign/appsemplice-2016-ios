//
//  RA_SelectedImageViewController.h
//  RestaurantApp
//
//  Created by World on 12/20/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KASlideShow.h"

@class RA_GalleryObject;

@interface RA_SelectedImageViewController : UIViewController<KASlideShowDelegate>
{
    IBOutlet UIImageView *containerImageView;
    IBOutlet UIActivityIndicatorView *activityIndicator;
}

@property (nonatomic, retain) UIImage *selectedImage;
@property (nonatomic, retain) RA_GalleryObject *gallaryObject;
@property (nonatomic, retain) NSArray *picturesArray;
@property (nonatomic, assign) int arrayIndex;

@end
