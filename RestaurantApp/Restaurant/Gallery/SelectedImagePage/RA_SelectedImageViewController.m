//
//  RA_SelectedImageViewController.m
//  RestaurantApp
//
//  Created by World on 12/20/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import "RA_SelectedImageViewController.h"
#import "RA_GalleryObject.h"
#import "RA_ImageCache.h"


@interface RA_SelectedImageViewController ()
{
    RA_ImageCache *imageCache;
    KASlideShow *_slideshow;
}

@end

@implementation RA_SelectedImageViewController

@synthesize selectedImage;
@synthesize gallaryObject;
@synthesize picturesArray;
@synthesize arrayIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    /*code added
    
    imageCache = [[RA_ImageCache alloc] init];
    
    _slideshow = [[KASlideShow alloc] initWithFrame:CGRectMake(0,0,320,250)];
    _slideshow.delegate = self;
    [_slideshow setDelay:3]; // Delay between transitions
    [_slideshow setTransitionDuration:1]; // Transition duration
    [_slideshow setTransitionType:KASlideShowTransitionSlide]; // Choose a transition type (fade or slide)
    [_slideshow setImagesContentMode:UIViewContentModeScaleAspectFill]; // Choose a content mode for images to display
    
   *///end code
    
  /*  activityIndicator.hidden = YES;
    
    imageCache = [[RA_ImageCache alloc] init];
    
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    //setting page's background color
    self.view.backgroundColor  = kPageBGColor;
    containerImageView.backgroundColor = [UIColor clearColor];
    containerImageView.contentMode = UIViewContentModeScaleAspectFit;
    containerImageView.image = self.selectedImage;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        [imageCache getImage:gallaryObject.fullImagePath];
    });

    self.view.backgroundColor = kPageBGColor;
    
    //creating left and right gesture
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
   

    
    //adding gestures to the imageview
    [containerImageView addGestureRecognizer:swipeLeft];
    [containerImageView addGestureRecognizer:swipeRight];
    containerImageView.userInteractionEnabled = YES;*/
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
  /*  RA_GalleryObject *item;
    int i;
    
        for (i = self.arrayIndex; i < self.picturesArray.count; i++) {
            
            item = [self.picturesArray objectAtIndex:i];
            
            [_slideshow addImage:[imageCache getImage:item.fullImagePath]];
            
        }

    // [_slideshow emptyAndAddImages:self.picturesArray]; // Add images from resources
    
    
    [_slideshow addGesture:KASlideShowGestureSwipe]; // Gesture to go previous/next directly on the image
    [self.view addSubview:_slideshow];*/
    
     activityIndicator.hidden = YES;
     
     imageCache = [[RA_ImageCache alloc] init];
     
     self.navigationController.navigationBar.translucent = NO;
     self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
     
     //setting page's background color
     self.view.backgroundColor  = kPageBGColor;
     containerImageView.backgroundColor = [UIColor clearColor];
     containerImageView.contentMode = UIViewContentModeScaleAspectFit;
     containerImageView.image = self.selectedImage;
     
    RA_GalleryObject *item = [self.picturesArray objectAtIndex:self.arrayIndex];
    containerImageView.image = [UIImage imageNamed:@"loading.gif"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
        containerImageView.image = [imageCache getImage:item.fullImagePath];
    });

     
     self.view.backgroundColor = kPageBGColor;
     
     //creating left and right gesture
     UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
     UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
     [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
     [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    
     //adding gestures to the imageview
     [containerImageView addGestureRecognizer:swipeLeft];
     [containerImageView addGestureRecognizer:swipeRight];
     containerImageView.userInteractionEnabled = YES;



}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

/**
 * Method name: handleSwipe
 * Description: handle transitions of images respect to swiping right or left
 */


-(void)handleSwipe:(UISwipeGestureRecognizer*)swipe
{
    BOOL goToNext = NO;
    
    if(swipe.direction == UISwipeGestureRecognizerDirectionRight)//if swipe right than show the previous image
    {
        if(self.arrayIndex > 0)
        {
            self.arrayIndex--;
        }
        else
        {
            return;
        }
    }
    else if (swipe.direction == UISwipeGestureRecognizerDirectionLeft)// if swipe left show the next image
    {
        if(self.arrayIndex < self.picturesArray.count - 1)
        {
            self.arrayIndex++;
            goToNext = YES;
        }
        else
        {
            return;
        }
    }
    
    //flipping images according to swipe direction. if swipe right to left then the flip will be from right to left and vice versa
    [UIView beginAnimations:nil context:nil];
    //[UIView setAnimationDuration:0.75];
    if(goToNext)
    {
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromRight;
       // [transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.view.layer addAnimation:transition forKey:@"transitionRight"];
       // [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:self.view cache:YES];
    }
    else
    {
        CATransition *transition = [CATransition animation];
        transition.duration = 0.25;
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        //[transition setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [self.view.layer addAnimation:transition forKey:@"transitionLeft"];
       // [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:self.view cache:YES];
    }
    
    RA_GalleryObject *item = [self.picturesArray objectAtIndex:self.arrayIndex];
    containerImageView.image = [UIImage imageNamed:@"loading.gif"];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.001 * NSEC_PER_SEC), dispatch_get_main_queue(), ^(void){
      containerImageView.image = [imageCache getImage:item.fullImagePath];
    });
	[UIView commitAnimations];
}


@end
