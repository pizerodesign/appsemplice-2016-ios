//
//  RA_GalleryObject.h
//  RestaurantApp
//
//  Created by World on 12/27/13.
//  Copyright (c) 2013 PizeroDesign. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RA_GalleryObject : NSObject

@property (nonatomic, assign) int objectId;
@property (nonatomic, retain) NSString *imagePath;
@property (nonatomic, retain) NSString *fullImagePath;
@property (nonatomic, retain) UIImage *thumbImage;
@property (nonatomic, retain) UIImage *fullImage;

@end
